class JeopardyGrid extends GenericGrid {
    constructor(options = {
        rowCount: 6,
        columnCount: 6,
        cellWidth: '50px',
        cellheight: '50px',
    }) {
        super(options)
        this.score = 0
        this.categoryIDs = ['19', '37', '133', '240', '252', '358']
        this.URL = 'https://jservice.kenzie.academy/api/clues?category='
        this.getData()
    }

    async getData() {
        const categoryPromises = this.categoryIDs.map(async id => {
            const res = await fetch(this.URL + id)
            return await res.json()
        })
        this.categories = await Promise.all(categoryPromises)

        this.assignClues(this.categories)
    }

    assignClues(categories) {
        for (let i = 0; i < this.matrix.length; i++) {
            for (let j = 0; j < this.matrix[i].length; j++) {
                let currentCell = this.matrix[j][i]
                let currentClue = categories[i].clues[j]
                if(currentClue.value !== j*200){
                    for(let k = 0; k < categories[i].clues.length; k++){
                        if(categories[i].clues[k].value === j*200){
                            currentClue = categories[i].clues[k]
                        }
                    }
                }
                if (j === 0) {
                    currentCell.element.innerHTML = currentClue.category.title
                } else {
                    currentCell.element.innerHTML = currentClue.value
                    currentCell.element.addEventListener('click', () => {
                        currentCell.element.innerHTML = currentClue.question
                        this.currentClue = currentClue
                        this.currentCell = currentCell
                    })
                }
            }
        }
    }

    submitHandler() {
        let userInput = document.getElementById('inputbox').value
        document.getElementById('inputbox').value = ''
        if (userInput.toLowerCase() === this.currentClue.answer.toLowerCase()) {
            this.score += this.currentClue.value
            document.getElementById('userpoints').innerHTML = this.score
            this.currentCell.element.innerHTML = this.currentClue.value
            this.currentCell.element.style.backgroundColor = 'darkgreen'
        } else {
            this.score -= this.currentClue.value
            document.getElementById('userpoints').innerHTML = this.score
            this.currentCell.element.innerHTML = this.currentClue.value
            this.currentCell.element.style.backgroundColor = 'darkred'
        }

    }

}