class GenericGrid {
    
    constructor (options = {
        rowCount: 6,
        columnCount: 6,
        cellWidth: '50px',
        cellheight: '50px',
    }) {
        this.options = options
        this.cellType = options.cellType || JeopardyCell
        this.matrix = []
    }

    createGridElement () {
        this.element = document.createElement('div')
        this.element.id = 'grid'
        for (let rowIndex = 0; rowIndex < this.options.rowCount; rowIndex++) {
            const rowElement = this.createRowElement(rowIndex)
            this.element.appendChild(rowElement)
            this.matrix[rowIndex] = []

            for(let cellIndex = 0; cellIndex < this.options.columnCount; cellIndex++) {
                const cell = this.createCell()
                rowElement.appendChild(cell.createCellElement(rowIndex, cellIndex))
                this.matrix[rowIndex][cellIndex] = cell
            }
        }
        return this.element
    }

    createCell () {
        return new JeopardyCell(this.options)
    }

    createRowElement (rowIndex) {
        const element = document.createElement('div')
        element.classList.add('row')
        element.dataset.rowIndex = rowIndex

        return element
    }

    addEventListeners (eventDescriptions) {
        for (let eventDescription of eventDescriptions) {
            const { listener, type, callback } = eventDescription
            listener.addEventListener(type, callback)
        }
    }
}