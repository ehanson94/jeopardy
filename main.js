const gridContainer = document.querySelector('#container')

const jeopardyGrid = new JeopardyGrid ({
    rowCount: 6,
    columnCount: 6,
    cellWidth: '150px',
    cellHeight: '150px',
    cellType: JeopardyCell,
})

const gridElement = jeopardyGrid.createGridElement()
gridContainer.appendChild(gridElement)

console.log(jeopardyGrid)
